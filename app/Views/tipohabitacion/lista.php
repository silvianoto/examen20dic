<?= $this->extend('templates/default') ?>


<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('tipohabitacion/alta')?>">Insertar</a>
        </li>
    </ul>


    <table class="table table-striped" id="myTable">
        <thead>
            <th>
                nombre
            </th>
            <th>
                descripcion
            </th>
            <th>
                tecnologia
            </th>
            <th>
                capacidad
            </th>
            <th>
                adultos
            </th>
            <th>
                ninyos
            </th>
        </thead>
       <tbody>
        <?php foreach ($tipohabitacion as $tipohabitacion): ?>
            <tr>
                <td>
                    <?= $tipohabitacion->nombre ?>
                </td>
                <td>
                    <?= $tipohabitacion->descripcion ?> 
                </td>
                <td>
                    <?= $tipohabitacion->tecnologia ?>
                </td>
                <td>
                    <?= $tipohabitacion->capacidad ?>
                </td>
                <td>
                    <?= $tipohabitacion->adultos ?>
                </td>
                <td>
                    <?= $tipohabitacion->ninyos ?>
                </td>
                <td class="text-right">
                    <a href="<?=site_url('tipohabitacion/actualiza/'.$tipohabitacion->id)?>" title="Editar <?= $tipohabitacion->nombre.' ' ?>">
                    <span class="bi bi-pencil-square"></span>
                    </a>
                    <a href="<?=site_url('tipohabitacion/borra/'.$tipohabitacion->id)?>" title="Borrar <?= $tipohabitacion->nombre.' ' ?>">
                    <span class="bi bi-pencil-square"></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>
<?= $this->endSection() ?>

