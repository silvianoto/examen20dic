<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of AlumnoModel
 *
 * @author jose
 */
class TipohabitacionModel extends Model{ //Ahora tengo un modelo, sino sería un clase normal sin superpoderes.
    protected $table = 'tipohabitacion';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir que campos son modificables.
    protected $allowedFields = ['nombre','descripcion','tecnologia','capacidad','adultos','ninyos'];
    protected $validationRules = [
        'nombre'    => 'required',
        'descripcion' => 'trim|required',
        'tecnologia'     => 'trim|min_length[6]',
        'capacidad'       => 'numeric',
        'adultos'       => 'numeric',
        'ninyos'       => 'numeric',
    ];
    
  
}
