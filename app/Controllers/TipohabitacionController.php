<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\TipohabitacionModel; //decimos donde está

use Config\Services;

/**
 * Description of AlumnoController
 *
 * @author jose
 */
class TipohabitacionController extends BaseController {

    public function index(){
        $tipohabitacionmodel = new TipoHabitacionModel();
        /*echo '<pre>';
        print_r($alumnoModel->findAll());
        echo '</pre>';*/
        $data['title'] = 'Listado de Habitaciones';
        $data['tipohabitacion'] = $tipohabitacionmodel->findAll();
        return view('tipohabitacion/lista',$data);
    }

    /*
    public function formInsertAlumno(){
        $data['title'] = 'Formulario Alta Alumnos';
        return view('alumno/formalta', $data);
    }
    
    //este método será llamado tras rellenar los campos del formulario y enviarlo.Esta función inserta datos en formulario
    public function insertAlumno(){
        //Tomar los datos del formulario
        $NIA = $this->request->getPost('NIA'); //es el valor del atributo name del campo input
        $nombre = $this->request->getPost('nombre');
        $apellido1 = $this->request->getPost('apellido1');
        $apellido2 = $this->request->getPost('apellido2');
        $nif = $this->request->getPost('nif');
        $email = $this->request->getPost('email');
        //lo pasamos a un array asociativo
        $alumno_nuevo = [
            'NIA' => $NIA,
            'nombre' => $nombre,
            'apellido1' => $apellido1,
            'apellido2' => $apellido2,
            'nif' => $nif,
            'email' => $email,
        ];
        //comprobamos lo que hemos recibido del formulario
        echo '<pre>';
        print_r($alumno_nuevo);
        echo '</pre>';
        //Si trabajamos con la base de datos necesitamos un modelo.
        //creamos el modelo que nos interesa
        $alumnoModel = new AlumnoModel();
        //usamos el método insert del modelo para añadir los datos de un alumno
        $alumnoModel->insert($alumno_nuevo);
        //Ahora queremos que siga añadiendo nuevos alumnos.nos direige al formulario
        return redirect()->to('alumno/formalta');
    }
*/    
    public function inserta(){
        $data['title'] = 'Formulario Alta Habitaciones';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('tipohabitacion/formalta', $data); 
        } else {
            $tipohabitacion_nuevo = [
            'nombre' => $this->request->getPost('nombre'),
            'descripcion' => $this->request->getPost('descripcion'),
            'tecnologia' => $this->request->getPost('tecnologia'),
            'capacidad' => $this->request->getPost('capacidad'),
            'adultos' => $this->request->getPost('adultos'),
            'ninyos' => $this->request->getPost('ninyos'),
        ];
            $tipohabitacionmodel = new TipohabitacionModel();
            if ($tipohabitacionmodel->insert($tipohabitacion_nuevo) === false){
               $data['errores'] = $tipohabitacionmodel->errors(); 
               return view('tipohabitacion/formalta', $data);  
            }
            
        }
    }
    
    
    /*
    public function formEdit($id){
        helper('form');
        $alumnoModel = new AlumnoModel();
        $data['alumno'] = $alumnoModel->find($id);
        $data['titulo'] = 'Modificar Alumno/a';
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        return view('alumno/formEdit',$data);
    }
    */
    
    public function actualiza($id){
        helper('form');
        $data['title'] = 'Modificar Tipo de Habitación';
        $tipohabitacionModel = new TipohabitacionModel();
        $data['tipohabitacion'] = $tipohabitacionModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('tipohabitacion/formEdit',$data);
        } else {
            $tipohabitacion = $this->request->getPost();
            unset($tipohabitacion['botoncito']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
        $tipohabitacionModel->setValidationRule('nombre', 'required|min_length[2]');
         if ($tipohabitacionModel->update($id, $tipohabitacion) === false){
                //hay un error al actualizar
                $data['errores'] = $tipohabitacionModel->errors();
                return view('tipohabitacion/formEdit',$data);
            }
        }        
        return redirect()->to('tipohabitacion/lista');
    }

    public function borra($id){
        helper('form');
        $data['title'] = 'Borrar Tipo de Habitación';
        $tipohabitacionModel = new TipohabitacionModel();
        $data['tipohabitacion'] = $tipohabitacionModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('tipohabitacion/formEdit',$data);
        } else {
            $tipohabitacion = $this->request->getPost();
            unset($tipohabitacion['botoncito']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
        $tipohabitacionModel->setValidationRule('nombre', 'required|min_length[2]');
         if ($tipohabitacionModel->delete($id, $tipohabitacion) === false){
                //hay un error al actualizar
                $data['errores'] = $tipohabitacionModel->errors();
                return view('tipohabitacion/formEdit',$data);
            }
        }        
        return redirect()->to('tipohabitacion/lista');
    }
}
